<xsl:stylesheet version= '1.0' 
                xmlns:xsl='http://www.w3.org/1999/XSL/Transform'> 
    <xsl:template match="/"> 
        <html> 
            <body> 
                <h1>
                    <xsl:text>Twitter @</xsl:text> 
                    <a>
                        <xsl:attribute name="href"> 
                            <xsl:value-of select="rss/channel/link"/> 
                        </xsl:attribute>
                        <xsl:value-of select="rss/channel/link"/> 
                    </a>    
                </h1>
                <table border="1">
                    <tr>
                        <th width="30">
                            <xsl:text>Title</xsl:text>
                        </th>   
                        <th width="20">
                            <xsl:text>Publication Date</xsl:text>
                        </th>  
                    </tr>    
                    
                    <xsl:for-each select="//item"> 
                        <tr> 
                            <td>
                                <a>
                                    <xsl:attribute name="href"> 
                                        <xsl:value-of select="link"/> 
                                    </xsl:attribute>
                                    <xsl:value-of select="title"/> 
                                </a>  
                            </td>
                            <td>
                                <xsl:value-of select="pubDate"/>
                            </td>    
                        </tr> 
                    </xsl:for-each>
                </table>
            </body>    
        </html> 
    </xsl:template> 
</xsl:stylesheet> 