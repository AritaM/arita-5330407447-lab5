<?xml version="1.0"?>
<xsl:stylesheet version= '1.0' 
                xmlns:xsl='http://www.w3.org/1999/XSL/Transform'> 
    <xsl:output method="html"/>
    <xsl:template match="/">
        <html xmlns = "http://www.w3.org/1999/xhiml">
            <head>
                <META http-equiv="Content-Type" content="text/html; charset=UTF-8"></META>
                <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"></meta>
                <title>Kanda Saikaew's MSN Log</title>
                <style>
                    body
                    { 
                    font-family: Verdana, arial,sans-serif;
                    }
                </style>
            </head>
            <body>
                <span style = "color: red">[Conversation started on 
                    <xsl:value-of select = "Log/Message[1]/@Date"/>
                    <xsl:text> </xsl:text>
                    <xsl:value-of select = "Log/Message[1]/@Time"/>]
                </span>
                <table border="0">
                    <xsl:for-each select="//Message">
                        <tr>
                            <td>[
                                <xsl:text> </xsl:text>
                                <xsl:value-of select = "@Date"/>
                                <xsl:text> </xsl:text>
                                <xsl:value-of select = "@Time"/>]
                            </td>
                            <xsl:if test="From/User[@FriendlyName=//Message[1]/From/User/@FriendlyName]">
                                <td>
                                    <span style="color: #FFAA00">
                                        <xsl:value-of select="//User/@FriendlyName"/>
                                    </span>
                                </td>
                                <td>:</td>
                                <td>
                                    <span style="color: #FFAA00">
                                        <xsl:value-of select="Text"/>
                                    </span>
                                </td>
                            </xsl:if>
                            <xsl:if test="From/User[@FriendlyName=//Message[2]/From/User/@FriendlyName]">
                                <td>
                                    <span style="color: #24913C">
                                        <xsl:value-of select="//User/@FriendlyName"/>
                                    </span>
                                </td>
                                <td>:</td>
                                <td>
                                    <span style="color: #24913C">
                                        <xsl:value-of select="Text"/>
                                    </span>
                                </td>
                            </xsl:if>
                        </tr>
                    </xsl:for-each>
                </table>
            </body>
        </html>
    </xsl:template>
</xsl:stylesheet> 